package main.java.queues.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ShopLine implements Runnable {
    private static Integer totalClients = 0;
    private static PrintWriter writer = null;
    private BlockingQueue<Client> clients;
    private Integer nrClients = 0;
    private Integer serviceTime = 0;
    private Integer totalWaitingTime = 0;
    private Integer id;

    public ShopLine(Integer id) {
        this.id = id;
        clients = new ArrayBlockingQueue<>(7);
    }

    public static void openLog() {
        try {
            writer = new PrintWriter("log.txt", "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closeLog() {
        writer.close();
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getLineSize() {
        return clients.size();
    }


    public synchronized void addClient(Client client) {
        try {
            clients.put(client);
            nrClients++;
            totalClients++;
            client.setId(totalClients);
            writer.println("Client " + client.getId() + " joined line no. " + this.id + "\n");
            serviceTime += client.getWaitingTime();
            totalWaitingTime += serviceTime;
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public float getAvgWaitingTime() {
        if (totalClients != 0) {
            return (totalWaitingTime / totalClients);
        }
        return 0;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (this.getLineSize() > 0) {
                    Thread.sleep(clients.peek().getWaitingTime());
                    Client client = clients.take();
                    writer.println("Client " + client.getId() + " left line no. " + this.id);
                    nrClients--;
                } else {

                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
