package main.java.queues.model;

import java.util.ArrayList;

public class Shop {

    private Thread shop;
    private Integer nrClient = 0;
    private Integer minArrivalTime;
    private Integer maxArrivalTime;

    private Integer stop = 0;
    private Thread generatorThread = new Thread(new ClientGenerator());

    private ShopLine firstLine = new ShopLine(1);
    private Thread firstLineThread = new Thread(firstLine);

    private ShopLine secondLine = new ShopLine(2);
    private Thread secondLineThread = new Thread(secondLine);

    private ShopLine thirdLine = new ShopLine(3);
    private Thread thirdLineThread = new Thread(thirdLine);

    private ArrayList<ShopLine> lines = new ArrayList<>(3);

    public Shop(Integer nrOfLines) {

        generatorThread.start();
        firstLineThread.start();
        secondLineThread.start();
        thirdLineThread.start();

        if (nrOfLines == 3) {
            lines.add(firstLine);
            lines.add(secondLine);
            lines.add(thirdLine);
        } else if (nrOfLines == 2) {
            lines.add(firstLine);
            lines.add(secondLine);
        } else if (nrOfLines == 1) {
            lines.add(firstLine);
        }

        shop = new Thread(() -> {

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            while (true) {
                Client client = ClientGenerator.generateClient();
                nrClient++;

                try {
                    Thread.sleep((long) (Math.random() * (maxArrivalTime - minArrivalTime) + minArrivalTime));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }

                Integer minSize = firstLine.getLineSize();
                for (ShopLine line : lines) {
                    if (line.getLineSize() < minSize) {
                        minSize = line.getLineSize();
                    }
                }

                for (ShopLine line : lines) {
                    if (line.getLineSize() == minSize) {
                        line.addClient(client);
                        break;
                    }
                }
            }
        });
        shop.start();
    }

    public void stopThreads() {
        Thread.currentThread().interrupt();
        firstLineThread.interrupt();
        secondLineThread.interrupt();
        thirdLineThread.interrupt();
        generatorThread.interrupt();
    }

    public ShopLine getFirstLine() {
        return firstLine;
    }

    public ShopLine getSecondLine() {
        return secondLine;
    }

    public ShopLine getThirdLine() {
        return thirdLine;
    }

    public void setArrivalTime(Integer min, Integer max) {
        minArrivalTime = min * 1000;
        maxArrivalTime = max * 1000;
    }

}
