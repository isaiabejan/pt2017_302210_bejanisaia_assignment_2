package main.java.queues.model;

public class ClientGenerator implements Runnable {
    private static Client client;
    private static Integer min = 1;
    private static Integer max = 1;

    public static void setWaitingTime(Integer min1, Integer max2) {
        min = min1 * 1000;
        max = max2 * 1000;
    }

    public static synchronized Client generateClient() {
        return client;
    }

    public void run() {
        while (true) {
            client = new Client((int) (Math.random() * (max - min)) + min);
        }
    }

}
