package main.java.queues.model;

public class Client {
    private Integer waitingTime;
    private Integer id;

    public Client(Integer time) {
        this.waitingTime = time;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWaitingTime() {
        return this.waitingTime;
    }

}
