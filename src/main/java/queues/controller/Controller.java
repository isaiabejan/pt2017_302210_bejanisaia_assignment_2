package main.java.queues.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.shape.Circle;
import main.java.queues.model.ClientGenerator;
import main.java.queues.model.Shop;
import main.java.queues.model.ShopLine;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    private Circle circle1;
    @FXML
    private Circle circle2;
    @FXML
    private Circle circle3;
    @FXML
    private Circle circle4;
    @FXML
    private Circle circle5;
    @FXML
    private Circle circle6;
    @FXML
    private Circle circle7;

    private ArrayList<Circle> firstLine = new ArrayList<Circle>(7);

    @FXML
    private Circle circle8;
    @FXML
    private Circle circle9;
    @FXML
    private Circle circle10;
    @FXML
    private Circle circle11;
    @FXML
    private Circle circle12;
    @FXML
    private Circle circle13;
    @FXML
    private Circle circle14;

    private ArrayList<Circle> secondLine = new ArrayList<Circle>(7);

    @FXML
    private Circle circle15;
    @FXML
    private Circle circle16;
    @FXML
    private Circle circle17;
    @FXML
    private Circle circle18;
    @FXML
    private Circle circle19;
    @FXML
    private Circle circle20;
    @FXML
    private Circle circle21;

    private ArrayList<Circle> thirdLine = new ArrayList<Circle>(7);

    @FXML
    private TextField lines;
    @FXML
    private TextField duration;
    @FXML
    private TextField minWaitingTime;
    @FXML
    private TextField maxWaitingTime;
    @FXML
    private TextField minArrivalTime;
    @FXML
    private TextField maxArrivalTime;
    @FXML
    private TextField avgFirstLine;
    @FXML
    private TextField avgSecondLine;
    @FXML
    private TextField avgThirdLine;

    private Shop shop;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstLine.add(circle1);
        firstLine.add(circle2);
        firstLine.add(circle3);
        firstLine.add(circle4);
        firstLine.add(circle5);
        firstLine.add(circle6);
        firstLine.add(circle7);

        secondLine.add(circle8);
        secondLine.add(circle9);
        secondLine.add(circle10);
        secondLine.add(circle11);
        secondLine.add(circle12);
        secondLine.add(circle13);
        secondLine.add(circle14);

        thirdLine.add(circle15);
        thirdLine.add(circle16);
        thirdLine.add(circle17);
        thirdLine.add(circle18);
        thirdLine.add(circle19);
        thirdLine.add(circle20);
        thirdLine.add(circle21);

        for (int i = 0; i < 7; i++) {
            firstLine.get(i).setVisible(false);
            secondLine.get(i).setVisible(false);
            thirdLine.get(i).setVisible(false);
        }

    }

    public synchronized void start() {

        shop = new Shop(Integer.parseInt(lines.getText()));
        ShopLine.openLog();

        Thread print = new Thread(() -> {

            ClientGenerator.setWaitingTime(Integer.parseInt(minWaitingTime.getText()), Integer.parseInt(maxWaitingTime.getText()));
            shop.setArrivalTime(Integer.parseInt(minArrivalTime.getText()), Integer.parseInt(maxArrivalTime.getText()));

            long startTime = System.currentTimeMillis();
            long elapsedTime = 0L;

            while (elapsedTime <= (long) (Integer.parseInt(duration.getText()) * 1000)) {

                if (lines.getText().equals("3")) {
                    drawCircles(shop.getFirstLine(), firstLine);
                    drawCircles(shop.getSecondLine(), secondLine);
                    drawCircles(shop.getThirdLine(), thirdLine);
                } else if (lines.getText().equals("2")) {
                    drawCircles(shop.getFirstLine(), firstLine);
                    drawCircles(shop.getSecondLine(), secondLine);
                } else if (lines.getText().equals("1")) {
                    drawCircles(shop.getFirstLine(), firstLine);
                }
                elapsedTime = (new Date()).getTime() - startTime;
            }

            avgFirstLine.setText("" + shop.getFirstLine().getAvgWaitingTime());
            avgSecondLine.setText("" + shop.getSecondLine().getAvgWaitingTime());
            avgThirdLine.setText("" + shop.getThirdLine().getAvgWaitingTime());

            this.stop();
            ShopLine.closeLog();
        });
        print.start();
    }

    public synchronized void drawCircles(ShopLine line, ArrayList<Circle> circles) {

        for (int i = 0; i < 7; i++) {
            if (i < line.getLineSize()) {
                circles.get(i).setVisible(true);
            } else {
                circles.get(i).setVisible(false);
            }
        }
    }

    public void stop() {
        shop.stopThreads();
        Thread.currentThread().interrupt();
    }

}